module.exports = {
    root: true,
    env: {
        node: true,
        browser: true,
        commonjs: true,
        es6: true,
        jest: true,
    },

    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'semi': ['error', 'always'],
        'quotes': [1, 'single'],
        'comma-dangle': ['warn', 'only-multiline'],
        'indent': ['error', 4],
        'prefer-arrow-callback': ['warn'],
        'no-throw-literal': ['error'],
        'arrow-parens': ['warn', 'always'],
        'max-len': ['warn', {
            code: 80,
            ignoreComments: true,
            ignoreUrls: true,
        }],
        'prefer-template': ['warn'],
        'no-array-constructor': 'off',
    },
    parserOptions: {
        parser: '@typescript-eslint/parser'
    }
};
