import path from 'path';

const envPath = process.env['CONFIG_PATH'];

const configPath = envPath ? path.resolve(envPath) : './dev-config.json';
export default require(configPath);
