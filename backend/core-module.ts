import Module from "./module";
import RpcHandler from './rpc-handler';
import moduleList from '@backend/module-loader';
import flatMap from 'lodash/flatMap';
import { StringFormatDefinition } from 'ajv';
import { RpcRegistry, DeviceTypeRegistry, TransportTypeRegistry} from '@backend/types';

abstract class CoreRpcHandler extends RpcHandler {
    protected deviceRegistry: DeviceTypeRegistry;
    protected transportRegistry: TransportTypeRegistry;
    constructor(deviceRegistry : DeviceTypeRegistry, transportRegistry : TransportTypeRegistry){
        super()
        this.deviceRegistry = deviceRegistry
        this.transportRegistry = transportRegistry;
    }
}

class AddDeviceRpcHandler extends CoreRpcHandler {
    constructor(deviceRegistry : DeviceTypeRegistry, transportRegistry : TransportTypeRegistry){
        super(deviceRegistry, transportRegistry)
    }
    async execute(execArgs: any): Promise<string> {
        let {deviceType, name, config, transportType, transportConfig} = execArgs;

        let tm = this.transportRegistry.getTransportManager(transportType);
        if(tm == null){
            throw new Error("No transport manager found for type " + transportType)
        }
        let transport = await tm.createTransport(transportType, transportConfig);
        
        let dm = this.deviceRegistry.getDeviceManager(deviceType);
        if(dm == null){
            throw new Error("No device manager found for type " + deviceType)
        }
        return dm.addDevice(deviceType, name, config, transport);
    }
    getActionId(): String {
        return "ADD_DEVICE";
    }

}

class UpdateDeviceRpcHandler extends CoreRpcHandler {
    constructor(deviceRegistry : DeviceTypeRegistry, transportRegistry : TransportTypeRegistry){
        super(deviceRegistry, transportRegistry)
    }
    
    execute(execArgs: any): Promise<void> {
        throw new Error("Method not implemented.");
    }
    getActionId(): String {
        return "UPDATE_DEVICE";
    }

}

class UpdateDeviceTransportRpcHandler extends CoreRpcHandler {
    constructor(deviceRegistry : DeviceTypeRegistry, transportRegistry : TransportTypeRegistry){
        super(deviceRegistry, transportRegistry)
    }
    
    execute(execArgs: any): Promise<void> {
        throw new Error("Method not implemented.");
    }
    getActionId(): String {
        return "UPDATE_DEVICE_TRANSPORT";
    }

}

class DeleteDeviceRpcHandler extends CoreRpcHandler {
    constructor(deviceRegistry : DeviceTypeRegistry, transportRegistry : TransportTypeRegistry){
        super(deviceRegistry, transportRegistry)
    }
    
    execute(execArgs: any): Promise<void> {
        throw new Error("Method not implemented.");
    }
    getActionId(): String {
        return "DELETE_DEVICE";
    }

}

export default class CoreModule implements Module {
    register(rpcRegistry: RpcRegistry, deviceRegistry: DeviceTypeRegistry, transportRegistry: TransportTypeRegistry): void {
        rpcRegistry.registerRpcHandler(new AddDeviceRpcHandler(deviceRegistry, transportRegistry));
        rpcRegistry.registerRpcHandler(new UpdateDeviceRpcHandler(deviceRegistry, transportRegistry));
        rpcRegistry.registerRpcHandler(new UpdateDeviceTransportRpcHandler(deviceRegistry, transportRegistry));
        rpcRegistry.registerRpcHandler(new DeleteDeviceRpcHandler(deviceRegistry, transportRegistry));
    }


}