import { DeviceTypeRegistry, TransportTypeRegistry, RpcRegistry } from '@backend/types';

interface Module {
    register(rpcRegistry : RpcRegistry, deviceRegistry : DeviceTypeRegistry, transportRegistry : TransportTypeRegistry) : void;
}

export default Module;
