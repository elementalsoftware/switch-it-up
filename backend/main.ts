'use strict';

import express from 'express';
import * as http from 'http';
import * as bodyParser from 'body-parser';
import RpcRouter from './rpc-router';
import SocketIO from 'socket.io';
import logger from '@backend/logger';
import appConfig from '@backend/config';
import flatten from 'lodash/flatten';
import moduleList from '@backend/module-loader';
import { DeviceManagerRegistry } from './device-type-registry';
import { TransportManagerRegistry } from './transport-type-registry';

const app = express();
const httpServer = new http.Server(app);
const socketServer = SocketIO(http);

const port = appConfig.port || 8080;

const rpcRouter = new RpcRouter();
const deviceRegistry = new DeviceManagerRegistry();
const transportRegistry = new TransportManagerRegistry();
flatten(moduleList).forEach(module => module.register(rpcRouter, deviceRegistry, transportRegistry));


const model = {};

app.use('/', express.static('dist'));
app.use(bodyParser.json());

app.get('/api', (req, res) => res.json(model));

app.get('/api/rpc', (req, res) => res.json(rpcRouter.getDescriptor()));

app.post('/api/rpc/:rpcId', (req, res) => {
    if (!rpcRouter.hasRpc(req.params.rpcId)) {
        res.status(404).json({ success: false, msg: 'rpc method ' + req.params.rpcId + ' is not available' });
        return;
    }
    rpcRouter.execRpc(req.params.rpcId, req.body).then(() => {
        res.json({ success: true });
    }).catch(err => {
        res.status(500).json({ success: false, msg: err });
    });
});

socketServer.on('connection', (socket) => {
    // TODO: send state, subscribe
    socket.on('rpc', (rpcId : String, requestId : any, args : any) => {
        if (!rpcRouter.hasRpc(rpcId)) {
            if (requestId) {
                socket.emit('rpc-result', requestId, false, 'rpc method ' + rpcId + ' is not available');
            }
            return;
        }
        rpcRouter.execRpc(rpcId, args).then(() => {
            if (requestId) {
                socket.emit('rpc-result', requestId, true);
            }
        }).catch(err => {
            if (requestId) {
                socket.emit('rpc-result', requestId, false, err.message);
            }
        });
    });
});

httpServer.listen(port, () => {
    logger.info('Application listening on ' + port);
});
