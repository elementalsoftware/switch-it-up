import Module from '@backend/module';
import { RpcRegistry, DeviceTypeRegistry, TransportTypeRegistry} from '@backend/types';
import { TcpTransportManager } from './transport-manager';

export default class TcpTransportModule implements Module {
    register(rpcRegistry: RpcRegistry, deviceRegistry: DeviceTypeRegistry, transportRegistry: TransportTypeRegistry): void {
        transportRegistry.registerTransportManager(new TcpTransportManager());
    }
}
