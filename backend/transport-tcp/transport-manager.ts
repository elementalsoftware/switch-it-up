import { TransportManager, Transport, TransportEventCallback } from '@backend/types';

class TcpTransport implements Transport {

    open(): Promise<void> {
        throw new Error("Method not implemented.");
    }
    close(): Promise<void> {
        throw new Error("Method not implemented.");
    }
    send(data: Buffer): Promise<void> {
        throw new Error("Method not implemented.");
    }
    onEvent(callback: TransportEventCallback): void {
        throw new Error("Method not implemented.");
    }
    
}

export class TcpTransportManager implements TransportManager {
    createTransport(transportType: string, config: any): Promise<Transport> {
        throw new Error("Method not implemented.");
    }
    getSupportedTransportTypes(): string[] {
        return ["tcp"];
    }

}