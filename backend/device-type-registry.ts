import { DeviceTypeRegistry, DeviceManager } from '@backend/types';

export class DeviceManagerRegistry implements DeviceTypeRegistry {

    private managers : Map<String, DeviceManager> = new Map();

    registerDeviceManager(manager: DeviceManager): void {
        manager.getSupportedDeviceTypes().forEach(type => {
            if(this.managers.has(type)){
                throw new Error("Device type " + type + " is already registered");
            }
            this.managers.set(type, manager);
        });
    }
    getDeviceManager(deviceType: string): DeviceManager {
        return this.managers.get(deviceType) || null;
    }
}