import Module from '@backend/module';

import Vp728Module from './device-vp728';
import CoreModule from './core-module';
import SerialTransportModule from './transport-serial';
import TcpTransportModule from './transport-tcp';

let moduleList : Module[] = [
    new CoreModule(),
    new Vp728Module(),
    new SerialTransportModule(),
    new TcpTransportModule(),
];

export default moduleList;
