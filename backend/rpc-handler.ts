import { JSONSchema } from '@backend/types';

export default abstract class RpcHandler {
    abstract execute(execArgs: any) : Promise<any>;

    abstract getActionId() : String;

    getValidationSchema () : JSONSchema {
        return {
            additionalProperties: true
        };
    }
}
