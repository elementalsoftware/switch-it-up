import RpcHandler from './rpc-handler';

export { JSONSchema } from "@redux/types"

export interface DeviceManager {
    addDevice(deviceType : string, name : string, config : any, transport : Transport) : Promise<DeviceId>;
    updateDevice(deviceId : DeviceId, name : string, newConfig : any) : Promise<void>;
    updateDeviceTransport(deviceId : DeviceId, newTransport : Transport) : Promise<void>;
    deleteDevice(deviceId : DeviceId) : void;
    getSupportedDeviceTypes() : string[];
}

export interface TransportEventCallback {
    onConnect() : void;
    onDisconnect(requested : boolean, reason : string) : void;
    onMessage(message : Buffer) : void;
}

export interface Transport {
    open() : Promise<void>;
    close() : Promise<void>;
    send(data : Buffer) : Promise<void>;
    onEvent(callback : TransportEventCallback) : void;
}

export interface TransportManager {
    createTransport(transportType : string, config : any) : Promise<Transport>;
    getSupportedTransportTypes() : string[];
}

export interface RpcRegistry {
    registerRpcHandler(handler : RpcHandler) : void ;
}

export interface DeviceTypeRegistry {
    registerDeviceManager(manager : DeviceManager) : void;
    getDeviceManager(deviceType : string) : DeviceManager;
}

export interface TransportTypeRegistry {
    registerTransportManager(manager : TransportManager) : void;
    getTransportManager(deviceType : string) : TransportManager;
}

export type DeviceId = string;
export type TransportId = string;
