import Ajv from 'ajv';
import RpcHandler from '@backend/rpc-handler';
import { JSONSchema } from '@backend/types';

let rpcHandlers = new Map<String, RpcHandler>();

export default class RpcRouter {
    private rpcHandlers : Map<String, RpcHandler>;
    private validators : Map<String, Ajv.ValidateFunction>;
    private ajv : Ajv.Ajv;

    constructor () {
        this.ajv = new Ajv();
        this.rpcHandlers = new Map<String, RpcHandler>();
        this.validators = new Map<String, Ajv.ValidateFunction>();
    }

    public registerRpcHandler(handler : RpcHandler) : void {
        if (this.hasRpc(handler.getActionId())) {
            throw new Error('Duplicate RPC found with ID ' + handler.getActionId());
        }
        this.rpcHandlers.set(handler.getActionId(), handler);
        this.validators.set(handler.getActionId(), this.ajv.compile(handler.getValidationSchema()));
    }

    public hasRpc (rpcId : String) : boolean {
        return rpcHandlers.has(rpcId);
    }

    public async execRpc (rpcId: String, rpcArgs: any) : Promise<void> {
        if (!this.hasRpc(rpcId)) {
            throw new Error('No RPC found with ID ' + rpcId);
        }
        let handler = <RpcHandler>rpcHandlers.get(rpcId);
        try {
            await this.validate(rpcId, rpcArgs);
            return handler.execute(rpcArgs);
        } catch (err) {
            throw new Error('Failed to validate arguments for RPC ' + rpcId + ': ' + err.message);
        }
    }

    private async validate (rpcId : String, execArgs : any) : Promise<void> {
        let validator = this.validators.get(rpcId);
        if (!validator) {
            throw new Error('No validator found for RPC ' + rpcId);
        }
        if (!validator(execArgs)) {
            throw new Error(this.ajv.errorsText(validator.errors));
        }
    }

    public getDescriptor () : Map<String, JSONSchema> {
        let mappingArray = Array.from(rpcHandlers.entries()).map((pair) : [String, JSONSchema] => {
            return [pair[0], pair[1].getValidationSchema()];
        });
        return new Map<String, JSONSchema>(mappingArray);
    }
};
