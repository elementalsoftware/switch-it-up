import { TransportTypeRegistry, TransportManager } from '@backend/types';

export class TransportManagerRegistry implements TransportTypeRegistry {

    private managers : Map<String, TransportManager> = new Map();

    registerTransportManager(manager: TransportManager): void {
        manager.getSupportedTransportTypes().forEach(type => {
            if(this.managers.has(type)){
                throw new Error("Transport type " + type + " is already registered");
            }
            this.managers.set(type, manager);
        });
    }
    getTransportManager(TransportType: string): TransportManager {
        return this.managers.get(TransportType) || null;
    }
}