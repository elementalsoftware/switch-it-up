import Module from '@backend/module';
import { RpcRegistry, DeviceTypeRegistry, TransportTypeRegistry} from '@backend/types';
import { SerialTransportManager } from './transport-manager';

export default class SerialTransportModule implements Module {
    register(rpcRegistry: RpcRegistry, deviceRegistry: DeviceTypeRegistry, transportRegistry: TransportTypeRegistry): void {
        transportRegistry.registerTransportManager(new SerialTransportManager());
    }
}
