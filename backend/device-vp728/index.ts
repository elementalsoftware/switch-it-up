import Module from '@backend/module';
import { RpcRegistry, DeviceTypeRegistry, TransportTypeRegistry} from '@backend/types';
import { Vp728DeviceManager } from './device-manager';
/**
 * Plan:
 * 
 * * This device module will register the device driver somehow
 * * There will be a core module implementing add / update / update transport / remove actions that will coordinate management of runtime objects
 * * That core module will call into some kind of device-specific device manager service (and/or transport manager service) to create these objects
 * * Devices will handle their own RPC / state management
 */

export default class Vp728Module implements Module {
    register(rpcRegistry: RpcRegistry, deviceRegistry: DeviceTypeRegistry, transportRegistry: TransportTypeRegistry): void {
        deviceRegistry.registerDeviceManager(new Vp728DeviceManager());
    }


}
