import { DeviceManager, Transport, DeviceId } from "@backend/types";

class Vp728Device {

}

export class Vp728DeviceManager implements DeviceManager {

    private activeDeviceMap = new Map<DeviceId, Vp728Device>();

    async addDevice(deviceType: string, name: string, config: any, transport: Transport): Promise<DeviceId> {
        return "";
    }
    updateDevice(deviceId: DeviceId, name: string, newConfig: any): Promise<void> {
        throw new Error("Method not implemented.");
    }
    updateDeviceTransport(deviceId: DeviceId, newTransport: Transport): Promise<void> {
        throw new Error("Method not implemented.");
    }
    deleteDevice(deviceId: DeviceId): void {
        throw new Error("Method not implemented.");
    }
    getSupportedDeviceTypes(): string[] {
        return ["vp728"];
    }

    
}