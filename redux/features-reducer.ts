import { AnyAction } from 'redux-starter-kit';
import deviceDriverReducer from './device-driver-reducer';
import commTransportReducer from './comm-transport-reducer';
import { DeviceDriverDefinition, CommTransportDefinition } from './types';

const initialState = {
    deviceDrivers: new Array<DeviceDriverDefinition>(),
    commTransports: new Array<CommTransportDefinition>(),
};

export default function featureReducer (state = initialState, action : AnyAction) {
    return {
        deviceDrivers: deviceDriverReducer(state.deviceDrivers, action),
        commTransports: commTransportReducer(state.commTransports, action),
    };
}
