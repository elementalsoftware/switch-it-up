import { createReducer } from 'redux-starter-kit';
import { DeviceDriverDefinition } from './types';

export default createReducer(new Array<DeviceDriverDefinition>(), {
    REGISTER_DEVICE_DRIVER: (state, action) => {
        state.push({
            name: action.name,
            config: action.config
        });
    }
});
