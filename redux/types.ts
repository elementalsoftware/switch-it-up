export type JSONSchema = object;

export interface DeviceDriverDefinition {
    name : string,
    config : JSONSchema,
}

export interface CommTransportDefinition {
    name : string,
    config : JSONSchema,
}

