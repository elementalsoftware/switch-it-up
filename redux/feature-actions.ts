import { JSONSchema } from './types';

export function createRegisterDeviceDriver (driverName : String, deviceConfig : JSONSchema) {
    return {
        type: 'REGISTER_DEVICE_DRIVER',
        name: driverName,
        config: deviceConfig
    };
}

export function createRegisterCommTransport (transportName : String, transportConfig : JSONSchema) {
    return {
        type: 'REGISTER_COMM_TRANSPORT',
        name: transportName,
        config: transportConfig
    };
}
