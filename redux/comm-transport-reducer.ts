import { createReducer } from 'redux-starter-kit';
import { CommTransportDefinition } from './types';

export default createReducer(new Array<CommTransportDefinition>(), {
    REGISTER_COMM_TRANSPORT: (state, action) => {
        state.push({
            name: action.name,
            config: action.config
        });
    }
});
