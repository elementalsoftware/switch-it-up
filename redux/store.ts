import { configureStore } from 'redux-starter-kit';
import featuresReducer from './features-reducer';

export default configureStore({
    reducer: {
        features: featuresReducer
    }
});
